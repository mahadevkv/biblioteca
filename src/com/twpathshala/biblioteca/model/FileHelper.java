package com.twpathshala.biblioteca.model;

import java.io.BufferedReader;
import java.io.IOException;

//Understands Reading file
public class FileHelper {
    BufferedReader bookReader;

    public FileHelper(BufferedReader bookReader) {
        this.bookReader = bookReader;
    }

    public Books readBooks() {
        Books books = new Books();
        try {
            String bookName;
            while ((bookName = bookReader.readLine()) != null) {
                books.add(createBook(bookName));
            }

        } catch (IOException e) {
        }
        return books;
    }

    private Book createBook(String bookDetails) {
        String details[] = bookDetails.split(",");
        String name = details[0];
        String author = details[1];
        int yearOfPublication = Integer.parseInt(details[2]);
        return new Book(name, author, yearOfPublication);
    }

    public Movies readMovies() {
        Movies movies = new Movies();
        try {
            String movieDetail;
            while ((movieDetail = bookReader.readLine()) != null) {
                movies.add(createMovie(movieDetail));
            }

        } catch (IOException e) {
        }
        return movies;
    }

    private Movie createMovie(String userDetail) {
        String details[] = userDetail.split(",");
        String name = details[0];
        int year = Integer.parseInt(details[1]);
        String director = details[2];
        int rating;
        if (details[3].equalsIgnoreCase("unrated")) {
            rating = 0;
        }
        rating = Integer.parseInt(details[3]);
        return new Movie(name, year, director, rating);
    }

    private User createUser(String movieDetail) {
        String details[] = movieDetail.split(",");
        String libraryNo = details[0];
        String password = details[1];
        String name = details[2];
        String email = details[3];
        String mobile = details[4];
        return new User(libraryNo, password, name, email, mobile);
    }

    public Users readUsers() {
        Users users = new Users();
        try {
            String userDetail;
            while ((userDetail = bookReader.readLine()) != null) {
                users.add(createUser(userDetail));
            }

        } catch (IOException e) {
        }
        return users;
    }
}
