package com.twpathshala.biblioteca.model;

import java.util.ArrayList;

//Understands group of Book
public class Books extends ArrayList<Book> {
    Book remove(String bookName) {
        Book book = new Book(bookName, "", 123);
        int index = this.indexOf(book);
        if (index == -1)
            return null;
        Book result = this.get(index);
        this.remove(index);
        return result;
    }

    boolean contains(String name) {
        Book book = new Book(name, "", 123);
        return this.contains(book);
    }

    @Override
    public String toString() {
        String bookString = "";
        for (Book book : this) {
            bookString += book + "\n";
        }
        return bookString;
    }

    Book get(String bookName) {
        Book book = new Book(bookName, "", 123);
        int index = this.indexOf(book);
        if (index == -1)
            return null;
        return this.get(index);
    }

    Books search(String searchTerm) {
        searchTerm = searchTerm.toLowerCase();
        Books result = new Books();
        for (Book book : this) {
            String bookDetails = book.toString().toLowerCase();
            if (bookDetails.contains(searchTerm)) {
                result.add(book);
            }
        }
        return result;
    }
}
