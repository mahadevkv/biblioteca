package com.twpathshala.biblioteca.model;

import java.util.ArrayList;

public class Users extends ArrayList<User> {
    public Users() {
    }

    public User validate(String libraryId, String password) {
        User user = new User(libraryId, password);
        int index = this.indexOf(user);
        if (index == -1)
            return InvalidUser.user;
        return this.get(index);
    }

    public User getInvalidUser() {
        return InvalidUser.user;
    }

    public static class InvalidUser {
        public static User user = new User();
    }
}
