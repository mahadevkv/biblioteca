package com.twpathshala.biblioteca.model;

//Understand Library Consumer Users
public class User {
    String libraryNumber;
    String password;
    private String name;
    private String email;
    private String mobile;

    public User() {
    }

    public User(String libraryNumber, String password) {
        this.libraryNumber = libraryNumber;
        this.password = password;
    }

    public User(String libraryNo, String password, String name, String email, String mobile) {

        this.libraryNumber = libraryNo;
        this.password = password;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "User{" +
                "libraryNumber='" + libraryNumber + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (libraryNumber != null ? !libraryNumber.equals(user.libraryNumber) : user.libraryNumber != null)
            return false;
        return password != null ? password.equals(user.password) : user.password == null;

    }
}
