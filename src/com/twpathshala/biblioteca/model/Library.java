package com.twpathshala.biblioteca.model;

import java.util.HashMap;

//Understands BookStore
public class Library {
    HashMap<Book, User> checkedOutBooks;
    HashMap<Movie, User> checkedOutMovies;
    Books books;
    Movies movies;

    public Library(Books books, Movies movies) {
        this.books = books;
        this.movies = movies;
        checkedOutBooks = new HashMap<>();
        checkedOutMovies = new HashMap<>();
    }

    public boolean isBookInLibrary(String bookName) {
        return books.contains(bookName);
    }

    public Book checkoutBook(String bookName, User currentUser) {
        Book book = books.get(bookName);
        checkedOutBooks.put(book, currentUser);
        return book;
    }

    public boolean returnBook(String bookName) {
        Book book = books.get(bookName);
        if (!books.contains(bookName) || !isCheckedOutBook(bookName))
            return false;
        checkedOutBooks.remove(book);
        return true;
    }

    public boolean isCheckedOutBook(String bookName) {
        if (!isBookInLibrary(bookName))
            return false;
        Book book = new Book(bookName, "", 123);
        return containsBook(book);
    }

    private boolean containsBook(Book book) {
        for (Book book1 :
                checkedOutBooks.keySet()) {
            if (book1.equals(book))
                return true;
        }
        return false;
    }

    public String books() {
        String toString = "";
        for (Book book : books) {
            if (!checkedOutBooks.containsKey(book))
                toString += book.toString() + "\n";
        }
        return toString;
    }

    public Books searchBook(String searchString) {
        return books.search(searchString);
    }

    public boolean isMovieInLibrary(String movieName) {
        return movies.contains(movieName);
    }

    public Movie checkoutMovie(String movieName, User currentUser) {
        Movie movie = movies.get(movieName);
        checkedOutMovies.put(movie, currentUser);
        return movie;
    }

    public String movies() {
        String toString = "";
        for (Movie movie : movies) {
            if (!checkedOutMovies.containsKey(movie))
                toString += movie.toString() + "\n";
        }
        return toString;
    }
}