package com.twpathshala.biblioteca.model;

import java.util.ArrayList;

//Understands group of Book
public class Movies extends ArrayList<Movie> {
    public Movies() {
    }

    Movie remove(String name) {
        Movie movie = new Movie(name, 1945, "", 2);
        int index = this.indexOf(movie);
        if (index == -1)
            return null;
        Movie result = this.get(index);
        this.remove(index);
        return result;
    }

    boolean contains(String name) {
        Movie movie = new Movie(name, 1945, "", 2);
        return this.contains(movie);
    }

    @Override
    public String toString() {
        String movieString = "";
        for (Movie movie : this) {
            movieString += movie + "\n";
        }
        return movieString;
    }

    Movie get(String name) {
        Movie movie = new Movie(name, 1945, "", 2);
        int index = this.indexOf(movie);
        if (index == -1)
            return null;
        return this.get(index);
    }

    Movies search(String searchTerm) {
        searchTerm = searchTerm.toLowerCase();
        Movies result = new Movies();
        for (Movie movie : this) {
            String bookDetails = movie.toString().toLowerCase();
            if (bookDetails.contains(searchTerm)) {
                result.add(movie);
            }
        }
        return result;
    }
}
