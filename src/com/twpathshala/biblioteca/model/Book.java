package com.twpathshala.biblioteca.model;

//Understand Library Entity
public class Book {
    private String bookName;
    private String author;
    private int yearOfPublication;

    public Book(String name, String author, int yearOfPublication) {
        this.bookName = name;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
    }

    @Override
    public String toString() {
        return String.format("%-35s %-30s %s", bookName, author, yearOfPublication);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return (bookName.equals(book.bookName));
    }
}
