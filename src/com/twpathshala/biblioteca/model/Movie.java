package com.twpathshala.biblioteca.model;

public class Movie {
    private String name;
    private int year;
    private String director;
    private int rating;

    Movie(String name, int year, String director, int rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }

    public String toString() {
        return String.format("%-35s %-30s %-10s %s", name, director, year, rating);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        return name != null ? name.equalsIgnoreCase(movie.name) : movie.name == null;

    }
}
