package com.twpathshala.biblioteca.view;

public class ConsoleOutput implements Output {
    public void show(String message) {
        System.out.println(message);
    }
}
