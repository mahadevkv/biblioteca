package com.twpathshala.biblioteca.view;

public interface Output {
    public void show(String message);
}
