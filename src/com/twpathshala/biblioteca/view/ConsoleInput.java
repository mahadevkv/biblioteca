package com.twpathshala.biblioteca.view;

import java.io.BufferedReader;
import java.io.IOException;

public class ConsoleInput implements Input {

    private BufferedReader reader;

    public ConsoleInput(BufferedReader bufferedReader) {

        reader = bufferedReader;
    }

    public int inputInt() {
        int option = 0;
        try {
            option = Integer.parseInt(reader.readLine());
        } catch (IOException e) {

        }
        return option;
    }

    public String inputString() {
        String string = "";
        try {
            string = reader.readLine();
        } catch (IOException e) {

        }
        return string;
    }
}
