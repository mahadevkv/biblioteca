package com.twpathshala.biblioteca.view;

public interface Input {
    int inputInt();

    String inputString();
}
