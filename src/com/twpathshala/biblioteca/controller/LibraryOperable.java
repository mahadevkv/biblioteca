package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.Book;
import com.twpathshala.biblioteca.model.Movie;

//Understands Contract of Library Operations.
public interface LibraryOperable {
    void returnBook();

    Book checkoutBook();

    void search();

    void listBooks();

    Movie checkoutMovie();

    void listMovies();

    void authenticate();

    void logout();

    String currentUser();

    boolean isUserLoggedIn();
}
