package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.Book;
import com.twpathshala.biblioteca.model.Books;
import com.twpathshala.biblioteca.model.Library;
import com.twpathshala.biblioteca.model.Movie;
import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;

//Understands LibraryOperable Contract
public class LibraryOperator implements LibraryOperable {
    Authenticator authenticator;
    Output output;
    Input input;
    Library library;

    public LibraryOperator(Output output, Input input, Library library, Authenticator authenticator) {
        this.output = output;
        this.input = input;
        this.library = library;
        this.authenticator = authenticator;
    }

    public void search() {
        output.show("Enter Book Name/Author name Or Year of Publication");
        String searchString = input.inputString();
        Books result = library.searchBook(searchString);
        if (result.size() == 0) {
            output.show("No Result Found!!");
            return;
        }
        output.show(result.toString());
    }

    public void returnBook() {
        output.show("Enter Book Name");
        String bookName = input.inputString();
        if (!library.isCheckedOutBook(bookName)) {
            output.show("That is not a valid book to return.");
            return;
        }
        library.returnBook(bookName);
        output.show("Thank you !");

    }

    public Book checkoutBook() {
        if (!authenticator.isUserLoggedIn()) {
            output.show("Not logged In,,,Please Log In to Avail this facility");
            return null;
        }
        output.show("Enter Book Name");
        String bookName = input.inputString();
        if (!library.isBookInLibrary(bookName)) {
            output.show("Book Is Not Available");
            return null;
        }
        Book book = library.checkoutBook(bookName, authenticator.getCurrentUser());
        if (book == null) {
            output.show("Already Checked Out");
            return null;
        }
        output.show("Enjoy the Book!");
        return book;
    }

    @Override
    public Movie checkoutMovie() {
        if (!authenticator.isUserLoggedIn()) {
            output.show("Not logged In,,,Please Log In to Avail this facility");
            return null;
        }
        output.show("Enter Movie Name");
        String MovieName = input.inputString();
        if (!library.isMovieInLibrary(MovieName)) {
            output.show("Movie Is Not Available");
            return null;
        }
        Movie movie = library.checkoutMovie(MovieName, authenticator.getCurrentUser());
        if (movie == null) {
            output.show("Already Checked Out");
            return null;
        }
        output.show("Enjoy the Movie!");
        return movie;
    }

    public void listBooks() {
        output.show(library.books());
    }

    public void listMovies() {
        output.show(library.movies());
    }

    @Override
    public void authenticate() {
        authenticator.authenticate();
    }

    @Override
    public void logout() {
        authenticator.logout();
    }

    @Override
    public String currentUser() {
        return authenticator.currentUser();
    }

    @Override
    public boolean isUserLoggedIn() {
        return authenticator.isUserLoggedIn();
    }
}
