package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.Books;
import com.twpathshala.biblioteca.model.FileHelper;
import com.twpathshala.biblioteca.model.Movies;
import com.twpathshala.biblioteca.model.Users;


public class DataLoader {
    FileHelper bookReader;
    FileHelper movieReader;
    FileHelper userLoader;

    public DataLoader(FileHelper bookReader, FileHelper movieReader, FileHelper userLoader) {
        this.bookReader = bookReader;
        this.movieReader = movieReader;
        this.userLoader = userLoader;
    }

    public Movies getMoviesList() {
        return movieReader.readMovies();
    }

    public Books getBookList() {
        return bookReader.readBooks();
    }

    public Users getUsersList() {
        return userLoader.readUsers();
    }
}
