package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.User;
import com.twpathshala.biblioteca.model.Users;
import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;

public class Authenticator {
    public static final int MAX_ATTEMPT = 3;
    static Users users;
    User currentUser;
    private Input input;
    private Output output;

    public Authenticator(Users users, Input input, Output output) {
        this.users = users;
        this.input = input;
        this.output = output;
        currentUser = Users.InvalidUser.user;
    }

    public Authenticator(Input input, Output output) {
        this.input = input;
        this.output = output;
    }

    boolean isUserLoggedIn() {
        return currentUser != Users.InvalidUser.user;
    }

    public String currentUser() {
        return currentUser.toString();
    }

    public User authenticate() {
        User user = users.getInvalidUser();
        for (int i = 0; i < MAX_ATTEMPT; i++) {
            output.show("Enter Library Id:");
            String libraryId = input.inputString();
            output.show("Enter Password");
            String password = input.inputString();
            user = users.validate(libraryId, password);
            User invaliduser = users.getInvalidUser();
            if (!user.equals(invaliduser)) {
                currentUser = user;
                return user;
            }
            output.show("Incorrect Id or Password");
            output.show("Attempt Remaining.." + (MAX_ATTEMPT - i - 1));
        }
        return user;
    }

    void logout() {
        output.show("Are You Sure , and Want to Logout from Biblioteca?? y/n");
        char c = input.inputString().charAt(0);
        if (c == 'y' || c == 'Y')
            currentUser = Users.InvalidUser.user;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
