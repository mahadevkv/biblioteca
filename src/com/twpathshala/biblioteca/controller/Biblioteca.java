package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;

//Understands Controlling Library
public class Biblioteca {
    private Output output;
    private Input input;

    private LibraryOperable libraryOperable;

    public Biblioteca(Output output, Input input, LibraryOperable libraryOperable) {
        this.output = output;
        this.input = input;
        this.libraryOperable = libraryOperable;
    }

    public void run() {
        output.show("Welcome to Biblioteca Application!");
        int exitOption = 0;
        int inputOption;
        do {
            output.show(getMenuString());
            inputOption = input.inputInt();
            if (!performAction(inputOption))
                output.show("Invalid Option!!!");
        } while (inputOption != exitOption);
    }

    private boolean performAction(int option) {
        switch (option) {
            case 1:
                libraryOperable.listBooks();
                break;
            case 2:
                libraryOperable.checkoutBook();
                break;
            case 3:
                libraryOperable.returnBook();
                break;
            case 4:
                libraryOperable.search();
                break;
            case 5:
                libraryOperable.listMovies();
                break;
            case 6:
                libraryOperable.checkoutMovie();
                break;
            case 7:
                libraryOperable.authenticate();
                break;
            case 8:
                libraryOperable.logout();
                break;
            case 9:
                output.show(libraryOperable.currentUser());
                break;
            case 0:
                output.show("Thank you for Using Biblioteca");
                break;
            default:
                return false;
        }
        return true;
    }

    private String getMenuString() {
        String menustring = "*****************************************************************";
        menustring = menustring + "\n 1 :List ALL  books\n 2: Checkout Book\n 3: Return Book\n 4:Search Book\n 5:List Movies\n 6:CheckOut Movie\n ";
        if (!libraryOperable.isUserLoggedIn())
            menustring = menustring + "7. LogIn\n ";
        else
            menustring = menustring + "8. Log Out\n 9: Viw My Profile\n ";
        menustring = menustring + "0 :Quit\n";
        menustring = menustring + "*****************************************************************";
        menustring = menustring + "\n  Choose Option!";
        return menustring;
    }
}