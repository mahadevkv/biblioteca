package com.twpathshala.biblioteca;

import com.twpathshala.biblioteca.controller.*;
import com.twpathshala.biblioteca.model.*;
import com.twpathshala.biblioteca.view.ConsoleInput;
import com.twpathshala.biblioteca.view.ConsoleOutput;
import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;

//Understands Start Of Application
public class BibliotecaApplication {
    static Authenticator authenticator;
    static Output output;
    FileHelper bookReader;
    FileHelper movieReader;
    FileHelper usersReader;
    BufferedReader consoleReader;
    Users users;
    DataLoader dataLoader;
    Input input;

    public static void main(String[] args) {
        BibliotecaApplication bibliotecaApplication = new BibliotecaApplication();
        bibliotecaApplication.initializeApp();
        bibliotecaApplication.startApp();
    }

    private void startApp() {
        Books books = dataLoader.getBookList();
        Movies movies = dataLoader.getMoviesList();
        Library library = new Library(books, movies);
        LibraryOperable libraryOperable = new LibraryOperator(output, input, library, authenticator);
        Biblioteca biblioteca = new Biblioteca(output, input, libraryOperable);
        biblioteca.run();
    }

    private void initializeApp() {
        try {
            bookReader = new FileHelper(new BufferedReader(new FileReader("resources/books")));
            movieReader = new FileHelper(new BufferedReader(new FileReader("resources/movies.txt")));
            usersReader = new FileHelper(new BufferedReader(new FileReader("resources/users.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        consoleReader = new BufferedReader(new InputStreamReader(System.in));
        input = new ConsoleInput(consoleReader);
        output = new ConsoleOutput();
        dataLoader = new DataLoader(bookReader, movieReader, usersReader);
        users = dataLoader.getUsersList();
        authenticator = new Authenticator(users, input, output);
    }
}