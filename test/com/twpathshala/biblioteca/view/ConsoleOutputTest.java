package com.twpathshala.biblioteca.view;

import com.twpathshala.biblioteca.model.Book;
import com.twpathshala.biblioteca.model.Books;
import com.twpathshala.biblioteca.model.Library;
import com.twpathshala.biblioteca.model.Movies;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ConsoleOutputTest {
    @Test
    public void showWithListOfBookBooksShouldPrintBooksCorrectly() {
        Book c = new Book("C", "mahadev", 1994);
        Book cpp = new Book("CPP", "shilkumar", 1994);
        Books books = new Books();
        books.add(c);
        books.add(cpp);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        Library library = new Library(books, new Movies());
        String expected1 = String.format("%-35s %-30s %s", "C", "mahadev", "1994");
        String expected2 = String.format("%-35s %-30s %s", "CPP", "shilkumar", "1994");
        new ConsoleOutput().show(library.books());
        assertEquals(expected1 + "\n" + expected2 + "\n\n", outputStream.toString());
    }

    @Test
    public void showWithStringMessageShouldPrintOutputCorrectly() {
        ConsoleOutput consoleOutput = new ConsoleOutput();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        consoleOutput.show("Test Message");
        assertEquals("Test Message\n", outputStream.toString());
    }

}
