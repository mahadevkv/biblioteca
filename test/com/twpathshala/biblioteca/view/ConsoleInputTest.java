package com.twpathshala.biblioteca.view;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ConsoleInputTest {
    @Test
    public void inputIntShouldReturnOneIfUserInput1() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("1");
        Input input = new ConsoleInput(bufferedReader);
        assertEquals(1, input.inputInt());
    }

    @Test
    public void inputStringShouldReturnNameIfUserInputName() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("Name");
        Input input = new ConsoleInput(bufferedReader);
        assertEquals("Name", input.inputString());
    }

    @Test
    public void inputIntShouldThrowExceptionIfUserInputString() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn("Name");
        Input input = new ConsoleInput(bufferedReader);
        try {
            input.inputInt();
            Assert.fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}
