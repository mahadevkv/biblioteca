package com.twpathshala.biblioteca.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoviesTest {
    Movies movies;
    Movie movie;

    @Before
    public void setUp() {
        movie = new Movie("blah", 1867, "mahadev", 3);
        movies = new Movies();
        movies.add(movie);
    }

    @Test
    public void removeShouldReturnMovieIfItIsPresetInList() {
        Movie result = movies.remove("blah");
        assertTrue(movie.equals(result));
    }

    @Test
    public void removeShouldReturnNullIfItIsNotPresetInList() {
        Movie result = movies.remove("secondBlah");
        assertNull(result);
    }

    @Test
    public void containsShouldReturnTrueIfNameIsPresetInList() {
        assertTrue(movies.contains("blah"));
    }

    @Test
    public void containsShouldReturnFalseIfNameIsNotPresetInList() {
        assertFalse(movies.contains("Second blah"));
    }

    @Test
    public void searchShouldReturnMatchingMoviesForStringPassed() {
        Movies searchResult = movies.search("bla");
        assertTrue(searchResult.contains("blah"));
    }

    @Test
    public void searchShouldReturnMatchingMoviesWithAutherNmaePassedAsSearchString() {
        Movie newMovie = new Movie("My Village my life", 1967, "mahadev", 10);
        movies.add(newMovie);
        Movies searchResult = movies.search("mahadev");
        assertTrue(searchResult.contains("blah"));
        assertTrue(searchResult.contains("My Village my life"));
    }

    @Test
    public void toStringShouldReturnFormatedStringFromMovies() {
        Movie newMovie = new Movie("My Village my life", 1967, "mahadev", 10);
        movies.add(newMovie);
        assertEquals("blah                                mahadev                        1867       3\n" +
                "My Village my life                  mahadev                        1967       10\n", movies.toString());
    }

    @Test
    public void getShouldReturnMovieWithSameNameWithPassedName() {
        Movie resultMovie = movies.get("blah");
        assertEquals(movie, resultMovie);
    }
}
