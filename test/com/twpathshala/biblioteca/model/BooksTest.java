package com.twpathshala.biblioteca.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class BooksTest {
    @Test
    public void removeShouldReturnBookIfItIsPresetInList() {
        Books books = new Books();
        Book book = new Book("blah", "mahade", 18234);
        books.add(book);
        Book result = books.remove("blah");
        assertTrue(book.equals(result));
    }

    @Test
    public void removeShouldReturnNullIfItIsNotPresetInList() {
        Books books = new Books();
        Book book = new Book("blah", "mahade", 18234);
        books.add(book);
        Book result = books.remove("secondBlah");
        assertNull(result);
    }

    @Test
    public void containsShouldReturnTrueIfNameIsPresetInList() {
        Books books = new Books();
        Book book = new Book("blah", "mahade", 18234);
        books.add(book);
        assertTrue(books.contains("blah"));
    }

    @Test
    public void containsShouldReturnFalseIfNameIsNotPresetInList() {
        Books books = new Books();
        Book book = new Book("blah", "mahade", 18234);
        books.add(book);
        assertFalse(books.contains("Second blah"));
    }

    @Test
    public void searchShouldReturnMatchingBooksForStringPassed() {
        Books books = new Books();
        Book book = new Book("14 days with Virrapan", "mahadev", 18234);
        books.add(book);
        Books searchResult = books.search("virrapan");
        assertTrue(searchResult.contains("14 days with Virrapan"));
    }

    @Test
    public void searchShouldReturnMatchingBooksWithAutherNmaePassedAsSearchString() {
        Books books = new Books();
        Book book = new Book("14 days with Virrapan", "mahadev", 18234);
        Book newBook = new Book("My Village my life", "mahadev", 18234);
        books.add(book);
        books.add(newBook);
        Books searchResult = books.search("mahadev");
        assertTrue(searchResult.contains("14 days with Virrapan"));
        assertTrue(searchResult.contains("My Village my life"));
    }

    @Test
    public void toStringShouldReturnFormatedStringFromBooks() {
        Books books = new Books();
        Book book1 = new Book("14 days with Virrapan", "mahadev", 18234);
        Book book2 = new Book("My Village my life", "mahadev", 18234);
        books.add(book1);
        books.add(book2);
        assertEquals("14 days with Virrapan               mahadev                        18234\n" +
                "My Village my life                  mahadev                        18234\n", books.toString());
    }
}
