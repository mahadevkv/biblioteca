package com.twpathshala.biblioteca.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class BookTest {
    @Test
    public void equalsShouldReturnTrueForSameObject() {
        Book book = new Book("Shambhu", "mahadev", 1994);
        assertEquals(book, book);
    }

    @Test
    public void equalsShouldReturnTrueForObjectsWithSameValue() {
        Book book = new Book("Shambhu", "mahadev", 1994);
        Book anotherBook = new Book("Shambhu", "mahadev", 1994);
        assertEquals(book, anotherBook);
    }

    @Test
    public void equalsShouldReturnFalseForObjectsWithoutSameValue() {
        Book book = new Book("Shambhu", "mahadev", 1994);
        Book anotherBook = new Book("Cpp", "mahadev", 1994);
        assertFalse(book.equals(anotherBook));
    }

    @Test
    public void equalsShouldReturnTrueForBookWithoutSameName() {
        Book book = new Book("Shambhu", "mahadev", 1994);
        Book anotherBook = new Book("Shambhu", "Gopi", 1994);
        assertTrue(book.equals(anotherBook));
    }
}
