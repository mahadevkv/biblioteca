package com.twpathshala.biblioteca.model;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UserTest {
    @Test
    public void equalsShouldReturnrTrueForSameUser() {
        User user = new User("1234-4321", "password");
        assertTrue(user.equals(user));
    }

    @Test
    public void equalsShouldReturnrTrueForUserWithSameIdAndPassword() {
        User user = new User("1234-4321", "password");
        User user1 = new User("1234-4321", "password");
        assertTrue(user.equals(user1));
    }
}
