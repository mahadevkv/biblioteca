package com.twpathshala.biblioteca.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LibraryTest {
    Books books;
    Book bookMundhewadi;
    Library library;

    @Before
    public void setup() {
        books = new Books();
        Book book = new Book("16 Days With Virrapan", "Mahadev", 2003);
        Book book1 = new Book("My Journy to US", "Mahadev", 2003);
        bookMundhewadi = new Book("Mundhewadi, the Village", "Mahadev", 2003);
        books.add(book);
        books.add(book1);
        books.add(bookMundhewadi);
        library = new Library(books, new Movies());
    }

    @Test
    public void checkoutShouldReturnBookIfCheckoutIsSuccessFull() {
        Book book3 = library.checkoutBook("Mundhewadi, the Village", new User());
        assertEquals(book3, bookMundhewadi);
    }

    @Test
    public void checkoutShouldReturnNullIfBookNotFound() {
        Book book3 = library.checkoutBook("The Jungle Book", new User());
        assertNull(book3);
    }

    @Test
    public void returnBookShouldReturnTrueIfSuccessfulReturn() {
        library.checkoutBook("16 Days With Virrapan", new User());
        assertTrue(library.returnBook("16 Days With Virrapan"));
    }

    @Test
    public void returnBookShouldReturnFalseIfBookIsNotCheckedOut() {
        assertFalse(library.returnBook("16 Days With Virrapan"));
    }

    @Test
    public void returnBookShouldReturnFalseIfBookIsNotBelongsToLibrary() {
        assertFalse(library.returnBook("Blah"));
    }

    @Test
    public void isCheckedOutShouldReturnTrueIfBookIsBelongsToLibraryAndCheckedOut() {
        library.checkoutBook("16 Days With Virrapan", new User());
        assertTrue(library.isCheckedOutBook("16 Days With Virrapan"));
    }

    @Test
    public void isCheckedOutShouldReturnFalseIfBookIsNOtBelongsToLibrary() {
        assertFalse(library.isCheckedOutBook(" blah"));
    }

    @Test
    public void isCheckedOutShouldReturnFalseIfBookIsNOtCheckedOutLibrary() {
        assertFalse(library.isCheckedOutBook("16 Days With Virrapan"));
    }

    @Test
    public void searchShouldReturnBooksIfBookIsInLibrary() {
        Books books = Mockito.mock(Books.class);
        Books expected = new Books();
        expected.add(new Book("14 days with Virrapan", "mahadev", 18234));
        when(books.search("virrapan")).thenReturn(expected);
        Library library = new Library(books, new Movies());
        Books searchResult = library.searchBook("virrapan");
        assertTrue(searchResult.contains("14 days with Virrapan"));
    }

    @Test
    public void isMovieInLibraryShouldReturnTrueIfMovieWithSameNameIsAvailable() {
        String movieName = "someMovie";
        Movies movies = mock(Movies.class);
        Library library = new Library(new Books(), movies);
        when(movies.contains(movieName)).thenReturn(true);
        assertTrue(library.isMovieInLibrary(movieName));
    }

    @Test
    public void checkOutMovieShouldReturnMovieRequestedIfAvailableAndAddToCheckedOutMovie() {
        String movieName = "requested Movie";
        Movies movies = mock(Movies.class);
        Movie movie = mock(Movie.class);
        when(movies.get(movieName)).thenReturn(movie);
        Library library = new Library(new Books(), movies);
        Movie result = library.checkoutMovie(movieName, new User());
        assertEquals(movie, result);
    }

}
