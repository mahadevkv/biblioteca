package com.twpathshala.biblioteca.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UsersTest {
    @Test
    public void loginShouldReturnInvalidUserIfInCorrectCredentialsPassed() {
        Users users = new Users();
        User user = users.validate("45245", "kjkjdvja");
        assertEquals(user, Users.InvalidUser.user);
    }
}

