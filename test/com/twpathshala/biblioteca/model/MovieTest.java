package com.twpathshala.biblioteca.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MovieTest {
    @Test
    public void toStringShouldReturnMovieInSpecificFormat() {
        Movie movie = new Movie("Sanam re", 2015, "Swapnil Gaikwad", 1);
        assertEquals("Sanam re                            Swapnil Gaikwad                2015       1", movie.toString());
    }

    @Test
    public void equalsShouldReturnTrueForBooksWithSameName() {
        Movie movie = new Movie("Sanam re", 2015, "Swapnil Gaikwad", 1);
        assertEquals(new Movie("Sanam re", 2010, "", 1), movie);
    }

}
