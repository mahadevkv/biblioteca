package com.twpathshala.biblioteca.model;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class FileHelperTest {

    @Test
    public void readBooksShouldReturnListOfBooksAvailableInFile() throws IOException {
        BufferedReader bookReader = Mockito.mock(BufferedReader.class);
        when(bookReader.readLine()).thenReturn("pragmatic programmer,Shilkumar Jadhav,1994").thenReturn(null);
        FileHelper fileHelper = new FileHelper(bookReader);
        ArrayList<Book> books = fileHelper.readBooks();
        assertEquals(1, books.size());
        Book book = books.get(0);
        assertEquals("pragmatic programmer                Shilkumar Jadhav               1994", book.toString());
    }

    @Test
    public void readMoviesShouldReturnListOfMoviesAvailableInFile() throws IOException {
        BufferedReader movieReader = Mockito.mock(BufferedReader.class);
        when(movieReader.readLine()).thenReturn("PK,2010,Dhananjay Khaparkuntikar,9").thenReturn(null);
        FileHelper fileHelper = new FileHelper(movieReader);
        Movies movies = fileHelper.readMovies();
        assertEquals(1, movies.size());
        Movie book = movies.get(0);
        assertEquals("PK                                  Dhananjay Khaparkuntikar       2010       9", book.toString());
    }
}
