package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.User;
import com.twpathshala.biblioteca.model.Users;
import com.twpathshala.biblioteca.view.ConsoleInput;
import com.twpathshala.biblioteca.view.ConsoleOutput;
import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticatorTest {
    @Test
    public void authenticateShouldReturnUserForCorrectUserAndPassword() {
        Output output = mock(ConsoleOutput.class);
        Input input = mock(ConsoleInput.class);
        Users users = mock(Users.class);
        when(input.inputString()).thenReturn("1234").thenReturn("password");
        User user = mock(User.class);
        when(users.validate("1234", "password")).thenReturn(user);
        Authenticator authenticator = new Authenticator(users, input, output);
        User result = authenticator.authenticate();
        assertEquals(user, result);
    }
}
