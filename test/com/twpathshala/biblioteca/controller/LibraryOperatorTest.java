package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.*;
import com.twpathshala.biblioteca.view.ConsoleInput;
import com.twpathshala.biblioteca.view.ConsoleOutput;
import com.twpathshala.biblioteca.view.Input;
import com.twpathshala.biblioteca.view.Output;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class LibraryOperatorTest {
    Output output;
    Input input;
    Library library;
    LibraryOperable libraryOperable;
    User user;
    Authenticator authenticator;

    @Before
    public void setUp() {
        output = Mockito.mock(ConsoleOutput.class);
        input = Mockito.mock(ConsoleInput.class);
        library = Mockito.mock(Library.class);
        user = new User("UserName", "Password");
        authenticator = mock(Authenticator.class);
        libraryOperable = new LibraryOperator(output, input, library, authenticator);
    }

    @Test
    public void returnBookShouldCallReturnBookOfLibraryIfBookIsValidToReturn() {
        when(input.inputInt()).thenReturn(3);
        when(input.inputString()).thenReturn("blah");
        when(library.isCheckedOutBook(any(String.class))).thenReturn(true);

        libraryOperable.returnBook();

        verify(library, atLeastOnce()).isCheckedOutBook("blah");
        verify(library, atLeastOnce()).returnBook("blah");
    }

    @Test
    public void returnBookShouldNotCallReturnBookOfLibraryIfBookIsNotValidToReturn() {
        when(input.inputInt()).thenReturn(3);
        when(input.inputString()).thenReturn("blah");
        when(library.isCheckedOutBook(any(String.class))).thenReturn(false);

        libraryOperable.returnBook();

        verify(library, atLeastOnce()).isCheckedOutBook("blah");
        verify(library, never()).checkoutBook("blah", user);
    }

    @Test
    public void checkOutBookShouldShowErrorMessageIfBookIsNotAvailable() {
        when(input.inputInt()).thenReturn(3);
        when(input.inputString()).thenReturn("blah");
        when(library.isBookInLibrary("blah")).thenReturn(false);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        libraryOperable.checkoutBook();

        verify(library, atLeastOnce()).isBookInLibrary("blah");
        verify(library, never()).checkoutBook("blah", user);
        verify(output, atLeastOnce()).show("Book Is Not Available");
    }

    @Test
    public void checkOutBookShouldCallCheckOutBookOfLibraryIfItIsValid() {
        when(input.inputInt()).thenReturn(3);
        when(input.inputString()).thenReturn("blah");
        when(library.isBookInLibrary("blah")).thenReturn(true);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        libraryOperable.checkoutBook();

        verify(library, atLeastOnce()).isBookInLibrary("blah");
        verify(library, atLeastOnce()).checkoutBook("blah", user);
    }

    @Test
    public void checkOutBookShouldShowEnjoyBookMessageIfSuccessfulCheckout() {
        Book book = mock(Book.class);

        when(input.inputInt()).thenReturn(3);
        when(input.inputString()).thenReturn("blah");
        when(library.isBookInLibrary("blah")).thenReturn(true);
        when(library.checkoutBook("blah", user)).thenReturn(book);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        libraryOperable.checkoutBook();

        verify(library, atLeastOnce()).isBookInLibrary("blah");
        verify(library, atLeastOnce()).checkoutBook("blah", user);
        verify(output, atLeastOnce()).show("Enjoy the Book!");
    }

    @Test
    public void searchBookShouldCallsearchBookOfLibrary() {
        Books books = mock(Books.class);

        when(input.inputInt()).thenReturn(4);
        when(input.inputString()).thenReturn("searchBook String");
        when(library.searchBook("searchBook String")).thenReturn(books);

        libraryOperable.search();
        verify(library, atLeastOnce()).searchBook("searchBook String");
    }

    @Test
    public void searchBookShouldShowMessageIfSearchReturnsEmptyList() {
        Books books = mock(Books.class);

        when(input.inputInt()).thenReturn(4);
        when(input.inputString()).thenReturn("searchBook String");
        when(books.size()).thenReturn(0);
        when(library.searchBook("searchBook String")).thenReturn(books);

        libraryOperable.search();
        verify(output, atLeastOnce()).show("No Result Found!!");
    }

    @Test
    public void searchBookShouldShowBooksIfSearchReturnsSomeBooks() {
        when(input.inputInt()).thenReturn(4);
        when(input.inputString()).thenReturn("searchBook String");

        Books books = mock(Books.class);

        when(books.size()).thenReturn(2);
        when(books.toString()).thenReturn("Search Result books");
        when(library.searchBook("searchBook String")).thenReturn(books);

        libraryOperable.search();

        verify(output, atLeastOnce()).show("Search Result books");
    }

    @Test
    public void listBookShouldListAllBooksFromLibraryByCallingLibraryBooks() {
        when(input.inputInt()).thenReturn(1);
        when(library.books()).thenReturn("All books from Library");

        libraryOperable.listBooks();

        verify(output, atLeastOnce()).show("All books from Library");
    }

    @Test
    public void listMovieShouldListAllMoviesFromLibraryByCallingLibraryMovies() {
        when(input.inputInt()).thenReturn(1);
        when(library.movies()).thenReturn("All Movies from Library");

        libraryOperable.listMovies();

        verify(output, atLeastOnce()).show("All Movies from Library");
    }

    //Tests on Operation of Movies
    @Test
    public void checkOutMovieShouldShowErrorMessageIfMovieIsNotAvailable() {
        when(input.inputInt()).thenReturn(5);
        when(input.inputString()).thenReturn("blah");
        when(library.isMovieInLibrary("blah")).thenReturn(false);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        libraryOperable.checkoutMovie();

        verify(output, atLeastOnce()).show("Movie Is Not Available");
    }

    @Test
    public void checkOutMovieShouldCallCheckOutMovieOfLibraryIfItIsValid() {
        when(input.inputInt()).thenReturn(5);
        when(input.inputString()).thenReturn("blah");
        when(library.isMovieInLibrary("blah")).thenReturn(true);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        libraryOperable.checkoutMovie();

        verify(library, atLeastOnce()).checkoutMovie("blah", user);
    }

    @Test
    public void checkOutMovieShouldShowEnjoyMovieMessageIfSuccessfulCheckout() {
        Movie movie = mock(Movie.class);
        when(authenticator.isUserLoggedIn()).thenReturn(true);
        when(authenticator.getCurrentUser()).thenReturn(user);
        when(input.inputInt()).thenReturn(5);
        when(input.inputString()).thenReturn("blah");
        when(library.isMovieInLibrary("blah")).thenReturn(true);
        when(library.checkoutMovie("blah", user)).thenReturn(movie);

        libraryOperable.checkoutMovie();

        verify(output, atLeastOnce()).show("Enjoy the Movie!");
    }

}

