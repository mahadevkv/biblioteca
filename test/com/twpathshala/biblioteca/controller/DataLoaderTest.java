package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.FileHelper;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class DataLoaderTest {
    @Test
    public void loadBooksShouldCallFileHelpersReadBooksMethod() {
        FileHelper bookReader = mock(FileHelper.class);
        FileHelper movieReader = mock(FileHelper.class);
        FileHelper usersLoader = mock(FileHelper.class);
        DataLoader dataLoader = new DataLoader(bookReader, movieReader, usersLoader);
        dataLoader.getBookList();
        verify(bookReader, atLeastOnce()).readBooks();
    }

    @Test
    public void loadMoviesShouldCallFileHelpersRedMovieMethod() {
        FileHelper bookReader = mock(FileHelper.class);
        FileHelper movieReader = mock(FileHelper.class);
        FileHelper usersReader = mock(FileHelper.class);
        DataLoader dataLoader = new DataLoader(bookReader, movieReader, usersReader);
        dataLoader.getMoviesList();
        verify(movieReader, atLeastOnce()).readMovies();
    }

}
