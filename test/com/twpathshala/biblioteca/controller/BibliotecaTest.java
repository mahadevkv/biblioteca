package com.twpathshala.biblioteca.controller;

import com.twpathshala.biblioteca.model.User;
import com.twpathshala.biblioteca.model.Users;
import com.twpathshala.biblioteca.view.ConsoleInput;
import com.twpathshala.biblioteca.view.ConsoleOutput;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class BibliotecaTest {
    ConsoleInput consoleInput;
    ConsoleOutput consoleOutput;
    LibraryOperable libraryOperable;
    Biblioteca biblioteca;
    Users users;

    @Before
    public void setUp() {
        consoleInput = Mockito.mock(ConsoleInput.class);
        consoleOutput = Mockito.mock(ConsoleOutput.class);
        libraryOperable = Mockito.mock(LibraryOperable.class);
        biblioteca = new Biblioteca(consoleOutput, consoleInput, libraryOperable);
        users = mock(Users.class);
    }

    @Test
    public void onInputOption1ItShouldCallListBooksOfLibraryOperable() {
        when(consoleInput.inputInt()).thenReturn(1).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).listBooks();
    }


    @Test
    public void onOption2ItShouldCallCheckoutBookOfLibraryOperable() {
        when(consoleInput.inputInt()).thenReturn(2).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).checkoutBook();
    }

    @Test
    public void onOption3ItShouldCallReturnBookOfLibraryOperable() {
        when(consoleInput.inputInt()).thenReturn(3).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).returnBook();
    }

    @Test
    public void onOption0ItShouldShowThankYouMessage() {
        when(consoleInput.inputInt()).thenReturn(0).thenReturn(0);
        biblioteca.run();
        verify(consoleOutput, atLeastOnce()).show("Thank you for Using Biblioteca");
    }

    @Test
    public void onOption4ItShouldShowCallSearchFunction() {
        when(consoleInput.inputInt()).thenReturn(4).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).search();
    }

    @Test
    public void onOption5ItShouldShowCallListMoviesFunction() {
        when(consoleInput.inputInt()).thenReturn(5).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).listMovies();
    }

    @Test
    public void onOption6ItShouldShowCallcheckOutMoviesFunction() {
        when(consoleInput.inputInt()).thenReturn(6).thenReturn(0);
        biblioteca.run();
        verify(libraryOperable, atLeastOnce()).checkoutMovie();
    }

    @Test
    public void onOption7ItShouldLoginToSystem() {
        Users users = mock(Users.class);
        when(consoleInput.inputInt()).thenReturn(7).thenReturn(0);
        when(consoleInput.inputString()).thenReturn("1234").thenReturn("password");
        User user = mock(User.class);
        when(users.validate("1234", "password")).thenReturn(user);
        when(users.getInvalidUser()).thenReturn(mock(User.class));
        when(libraryOperable.isUserLoggedIn()).thenReturn(true);
        biblioteca.run();
        assertTrue(libraryOperable.isUserLoggedIn());
    }

    @Test
    public void onOption8ItShouldLogOutFromSystem() {
        when(consoleInput.inputInt()).thenReturn(8).thenReturn(0);
        when(consoleInput.inputString()).thenReturn("y");
        new Authenticator(consoleInput, consoleOutput).logout();
        biblioteca.run();
        assertFalse(libraryOperable.isUserLoggedIn());
    }


    @Test
    public void inValidInputOptionFromUserShouldPrintMessage() {
        int invalidOption = 15;
        when(consoleInput.inputInt()).thenReturn(invalidOption).thenReturn(0);
        biblioteca.run();
        verify(consoleOutput, atLeastOnce()).show("Invalid Option!!!");
    }

}
